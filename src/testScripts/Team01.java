package testScripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import generic_Library.Browser;
import generic_Library.Constants;
import generic_Library.WebdriverComLib;
import objectRepository.Create_Team;
import objectRepository.Login;
@Listeners(generic_Library.SampleListner.class)

public class Team01 extends WebdriverComLib
{
	WebDriver driver;

	@Test
	public void execute()
	{
		
			driver=Browser.getBrowser();
			driver.manage().window().maximize();
			
			wait_for_page_load_implicitwait();
			driver.get(Constants.url);
			
			Login lp=PageFactory.initElements(driver, Login.class);
			lp.Login_Dream11("chmurali9999@gmail.com", "12345678");
			
			
			Create_Team ct=PageFactory.initElements(driver, Create_Team.class);
			ct.create_team();
	
		
	}
}

	
	

