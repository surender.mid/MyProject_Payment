package generic_Library;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Browser {

	
	public static WebDriver driver;
	
	
	public static WebDriver getBrowser()
	{
		if(Constants.browser.equals("firefox"))
		{
			driver=new FirefoxDriver();
		}
		
		else if(Constants.browser.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\selenium advanced\\chromedriver.exe");
			driver=new ChromeDriver();
		}
		else
		{
			System.setProperty("webdriver.ie.driver", "C:\\selenium advanced\\IEDriverServer.exe");
			driver=new InternetExplorerDriver();
		}
		return driver;
		
	}
		
}
