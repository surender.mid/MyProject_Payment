package generic_Library;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelLib {

	
	String testdata_path=System.getProperty("user.dir")+"\\Testdata\\testdata.xlsx";
	
	public String get_excel_Data(String sheet_Name,int row_Number,int col_Number) throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		FileInputStream fis=new FileInputStream(testdata_path);
		Workbook wb=WorkbookFactory.create(fis);
		Sheet sh=wb.getSheet(sheet_Name);
		Row rw=sh.getRow(row_Number);
		Cell cl=rw.getCell(col_Number);
		String data=cl.getStringCellValue();
		
		return data;
		
	}
}
