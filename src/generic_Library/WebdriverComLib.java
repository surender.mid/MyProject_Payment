package generic_Library;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class WebdriverComLib {

	
	public void click(WebElement wb)
	{
		wb.click();
	}
	public void click(WebElement wb,String data)
	{
		Select sel=new Select(wb);
		sel.selectByVisibleText(data);
	}
	public void click(WebElement wb,int index)
	{
		Select sel=new Select(wb);
		sel.selectByIndex(index);
	}
	public void click_By_Value(WebElement wb,String value)
	{
		 Select sel=new Select(wb);
		 sel.selectByValue(value);
	}
	public void type(WebElement we,String data)
	{
		we.sendKeys(data);
	}
	public  static void wait_for_page_load_implicitwait()
	{
		Browser.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	
}
