package objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import generic_Library.WebdriverComLib;

public class Create_Team extends WebdriverComLib 
{

	
	@FindBy(xpath="//a[text()='Create Your Team']")
	WebElement Create_Team;
	
	@FindBy(xpath="//a[contains(text(),'WK')]")
	WebElement Sel_Wiki_Category;
	
	@FindBy(xpath="//a[contains(text(),'WK')]/../../following-sibling::p[contains(text(),'Select only 1 player from the list below.')]/../div[2]/div/div[@data-ptype='WK'][1]")
	WebElement sel_Wiki01;
	
	@FindBy(xpath="//ul/li[2]/a[contains(text(),'Batsmen')]")
	WebElement Sel_Batsmen_Category;
	
	@FindBy(xpath="//a[contains(text(),'Batsmen')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below')]/../div[2]/div/div[@data-ptype='BAT'][1]")
	WebElement Sel_Batsmen01;
	
	@FindBy(xpath="//a[contains(text(),'Batsmen')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below')]/../div[2]/div/div[@data-ptype='BAT'][2]")
	WebElement Sel_Batsmen02;
	
	@FindBy(xpath="//a[contains(text(),'Batsmen')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below')]/../div[2]/div/div[@data-ptype='BAT'][3]")
	WebElement Sel_Batsmen03;
	
	
	@FindBy(xpath="//ul/li[3]/a[contains(text(),'All-Rounders')]")
	WebElement All_Rounder_Category;
	
	@FindBy(xpath="//a[contains(text(),'All-Rounders')]/../../following-sibling::p[contains(text(),'Select min 1 & max 3 players from the list below.')]/../div[2]/div/div[@data-ptype='ALL'][1]")
	WebElement All_rounder01;
	
	@FindBy(xpath="//a[contains(text(),'All-Rounders')]/../../following-sibling::p[contains(text(),'Select min 1 & max 3 players from the list below.')]/../div[2]/div/div[@data-ptype='ALL'][2]")
	WebElement  All_rounder02;
	
	@FindBy(xpath="//ul/li[4]/a[contains(text(),'Bowlers')]")
	WebElement Sel_Bowlers_Category;
	
	@FindBy(xpath="//a[contains(text(),'Bowlers')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below.')]/../div[2]/div/div[@data-ptype='BOWL'][6]")
	WebElement Sel_Bowler01;
	
	@FindBy(xpath="//a[contains(text(),'Bowlers')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below.')]/../div[2]/div/div[@data-ptype='BOWL'][7]")
	WebElement Sel_Bowler02;
	
	@FindBy(xpath="//a[contains(text(),'Bowlers')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below.')]/../div[2]/div/div[@data-ptype='BOWL'][3]")
	WebElement Sel_Bowler03;
	
	@FindBy(xpath="//a[contains(text(),'Bowlers')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below.')]/../div[2]/div/div[@data-ptype='BOWL'][4]")
	WebElement Sel_Bowler04;
	
	@FindBy(xpath="//a[contains(text(),'Bowlers')]/../../following-sibling::p[contains(text(),'Select min 3 & max 5 players from the list below.')]/../div[2]/div/div[@data-ptype='BOWL'][5]")
	WebElement Sel_Bowler05;
	
	@FindBy(xpath="//div[contains(text(),'Captain gets 2x the points')]")
	WebElement Sel_Captain_Button;
	
	@FindBy(xpath="//div[@id='myDropdown']/ul/li[1]/div/div[@class='dd-img-wrapper batImg'][1]")
	WebElement Sel_Captain;
	
	@FindBy(xpath="//div[contains(text(),'Vice Captain gets 1.5x the points')]")
	WebElement Sel_ViceCaptain_Button;
	
	@FindBy(xpath="//div[@id='myDropdown2']/ul/li[2]")
	WebElement Sel_ViceCaptain;
	
	@FindBy(xpath="//div/a[@id='saveCVC']")
	WebElement Save_Team;
	
	public void create_team()
	{
		click(Create_Team);
		
		click(Sel_Wiki_Category);
		click(sel_Wiki01);
		click(Sel_Batsmen_Category);
		click(Sel_Batsmen01);
		click(Sel_Batsmen02);
		click(Sel_Batsmen03);
		click(All_Rounder_Category);
		click(All_rounder01);
		click(All_rounder02);
		click(Sel_Bowlers_Category);
		click(Sel_Bowler01);
		click(Sel_Bowler02);
		click(Sel_Bowler03);
		click(Sel_Bowler04);
		click(Sel_Bowler05);
		click(Sel_Captain_Button);
		click(Sel_Captain);
		click(Sel_ViceCaptain_Button);
		click(Sel_ViceCaptain);
		click(Save_Team);
		
		
		
		
		
	}
}
