package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.FindBy;

import generic_Library.WebdriverComLib;

public class Login extends WebdriverComLib
{
	
	@FindBy(xpath="//input[@id='m_txtEmailId']")
	WebElement UserName;
	
	@FindBy(xpath="//input[@id='m_txtPassword']")
	WebElement PassWord;
	
	@FindBy(xpath="//a[@id='userLoginClick']")
	WebElement Login_Button;
	
	@FindBy(xpath="//span[@class='indLink']")
	WebElement Sel_Country;
	
	@FindBy(xpath="//p[contains(text(),'Cricket')]")
	WebElement Sel_Game;
	
	public void Login_Dream11(String username,String password)
	{
		try
		{
		click(Sel_Country);
		click(Sel_Game);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		
		type(UserName, username);
		type(PassWord, password);
		click(Login_Button);
	}
	
	

}
