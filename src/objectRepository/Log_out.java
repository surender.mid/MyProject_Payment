package objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Log_out {

	
	@FindBy(xpath="//span[contains(text(),'Lucky89')]")
	WebElement UN_Logout;
	
	@FindBy(xpath="//a[contains(text(),'Logout')]")
	WebElement Logout;
	
	public void LogOut()
	{
		UN_Logout.click();
		Logout.click();
	}
}
